SELECT movie.mov, movie.mov_title, movie_cast.role, actor.act_fname, actor.act_lname
FROM movie, movie_cast, actor, movie_genres, genres
WHERE (
  movie.mov_id=movie_cast.mov_id
  AND movie_cast.act_id=actor.act_id
  AND movie.mov_id=movie_genres.mov_id
  AND movie_genres.gen_id=genres.gen_id
  OR genres.genres_title="Sci-Fi"
  AND movie.lang="English"
  AND movie.year>='1900'
  AND movie.year<'2000';
)

