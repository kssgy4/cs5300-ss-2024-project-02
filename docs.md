# Project 2: Heuristic Query Optimization
## Requirements
Needs a python interpreter to run (python3.8 or above)

**Input:** Name of the SQL file which needs to be optimized

**Output:** Returns the following lists:
- List of selection conditions
- List of projection conditions
- List of tables and their attributes

## Code flow and methodology

This programming project implements an SQL parser and the corresponding optimization tree to optimize SQL code. Here's an outline for how the code works:
1. Parses the SQL query into selection and projection conditions
2. Uses the information from parsing to create a query tree (incomplete)
3. Optimizes the query tree using the optimization algorithm (incomplete)
4. Converting the optimized query tree into an SQL statement (incomplete)

### 1. Parsing the SQL query
The query parser is implemented in a manner similar to a recursive descent parser. The following grammar is used to implement the parser:

S $\rightarrow$ `SELECT` A `FROM` B

A $\rightarrow$ `TableName.ColName,` A | `TableName.ColName,` where `ColName` is the name of the attribute in the corresponding table `TableName`

B $\rightarrow$ `TableName` C | `TableName,` B

C $\rightarrow$ $\varepsilon$ | `WHERE` D

D $\rightarrow$ `TableName.ColName<op>TableName.ColName` E where `<op>` is an operator

E $\rightarrow$ `AND` D | `OR` D | $\varepsilon$

Some notes:
- The parser can only accept a limited subset of possible SQL queries.
- For rule A, `colName` MUST be specified with the table it belongs to.
- For rule D, there must be no spaces between the operator and the left-hand-side and the right-hand-side statements. Example: `Table1.Attr1 = Table2.Attr1` won't be accepted by the parser.
- The parser cannot accept nested queries.

### 2. Creating a query tree
Although this part of the project is incomplete, it would be implemented as follows:
- Create a root node, and add the projection conditions to its `data` attribute
- Create a child, and then add the selection attributes to `data`
- For the tables, a left-deep recursive tree would be implemented, and the table names would be added to the leaf nodes.

### 3. Optimizing the query tree
- Selection conditions would be placed using DFS. If a selection condition applies to a specific leaf of a tree, the node with the selection condition would be the parent of the leaf node.
- The selection conditions would then be applied to cross products to create joins.

### 4. Creating the optimized SQL query
- The information from the query tree would then be used to create an optimized SQL query.


