import sql_tree
from typing import List, Dict

class QueryParser():
	def __init__(self, tokens):
		"""
		Create a new QueryParser object with the following attributes:
		- self.tokens: input is read from file and each string in the file is parsed
					   into a list.
		- self.tables_attrs: dictionary of tables, which maps to their attributes
		- self.pi_list: list of projections conditions from the query (extracted from
						the WHERE clause)
		- self.sigma_list: list of selection conditions (column names after SELECT)
		- self.tkn_indx: used to access one token to the next.
		"""

		self.tokens : List[str] = tokens

		self.tables_attrs : Dict[str, List[str]] = {}
		self.pi_list : List[str] = []
		self.sigma_list : List[str] = []

		self.tkn_indx : int = 0

	def parse_tokens(self) -> None:
		"""
		Starts the parser. Implements the rule S -> 'SELECT' A 'FROM' B
		"""

		if self.tokens[self.tkn_indx] == 'SELECT':
			self.tkn_indx+=1
			self.parse_A()

		if self.tokens[self.tkn_indx] == 'FROM':
			self.tkn_indx+=1
			self.parse_B()

	def parse_A(self) -> None:
		"""
		Processes the projection conditions after the SELECT statement and parses
		them into 'self.pi_list'.
		Implements the rule A -> 'TableName.ColName,' A | 'TableName.ColName'
		"""
		
		proj_cond : str = self.tokens[self.tkn_indx]

		if proj_cond == '*':
			self.pi_list.append(proj_cond)
			self.tkn_indx+=1
			return

		elif proj_cond[-1] == ',':
			self.pi_list.append(proj_cond[0:-1])
			table_name, table_attr = proj_cond[0:-1].split('.')

			if table_name not in self.tables_attrs:
				self.tables_attrs[table_name] = [table_attr]
			else:
				self.tables_attrs[table_name].append(table_attr)

			self.tkn_indx+=1
			self.parse_A()
			return
		
		else:
			self.pi_list.append(proj_cond)
			self.tkn_indx+=1
			return

	def parse_B(self):
		"""
		Processes the selection conditions after the SELECT statement and parses them into self.sigma_list.
		Implements the rule B -> 'TableName,' B | 'TableName' C
		"""
		
		table_name = self.tokens[self.tkn_indx]

		if table_name[-1] == ',':
			self.tkn_indx+=1
			self.parse_B()
		
		else:
			self.tkn_indx+=1
			self.parse_C()
			return
		
	def parse_C(self):
		"""
		Ends the parsing, or calls parse_D. Implements the rule C -> epsilon | 'WHERE' D
		"""

		if self.tkn_indx == len(self.tokens):
			return
		else:
			self.tkn_indx+=1
			self.tkn_indx+=1 # Skipping the parentheses after WHERE
			self.parse_D()
	
	def parse_D(self):
		"""
		Parses conditions in the WHERE clause. Implements the rules
		D -> 'TableName.ColName=TableName.ColName' E
		E -> 'AND' D | 'OR' D | epsilon
		"""
		cond_statement = self.tokens[self.tkn_indx]

		self.sigma_list.append(cond_statement.strip(','))
		self.tkn_indx+=1

		if self.tokens[self.tkn_indx] == 'AND' or self.tokens[self.tkn_indx] == 'OR':
			self.tkn_indx+=1
			self.parse_D()
		
		return
	
	def print_info(self):
		print("Projection conditions:", self.pi_list)
		print("Selection conditions:", self.sigma_list)
		print("Tables and attributes:", self.tables_attrs)


def read_file(filename : str) -> List[str]:
	"""
	Reads the sql file and creates a list of tokens
	"""
	with open(filename) as sqlfile:
		input_lines = sqlfile.read().split('\n')
	
	tokens = []
	for line in input_lines:
		tokens = tokens + line.strip().split(' ')
	
	return tokens

if __name__ == "__main__":
	
	filename = input("Enter the filename: ")
	
	query_tokens = read_file(filename)

	myparser = QueryParser(query_tokens)
	myparser.parse_tokens()
	myparser.print_info()

